# Django multilanguage app #

### Installation ###

You need to configure your settings file 

*** We need to add 'django.middleware.locale.LocaleMiddleware' in the middleware classes, this middleware need to be after 'django.contrib.sessions.middleware.SessionMiddleware' like this:**

    MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
     )

*** Specify the traductions that we need and our local language code**

    LANGUAGE_CODE = 'es'

   gettext = lambda s: s

   LANGUAGES = (
      ('es',  gettext('Spanish')),
      ('zh',  gettext('Chinese')),
      ('en',  gettext('English')),
   )

  

*** Define the path of the traductions "locale"**

    LOCALE_PATHS = (
     os.path.join(BASE_DIR, "locale"),
    )
 
*** define the context processor for i18n**

    from django.conf import global_settings
    TEMPLATE_CONTEXT_PROCESSORS = global_settings.TEMPLATE_CONTEXT_PROCESSORS + ('django.core.context_processors.i18n',)

*** Define url for i18n** 

    url(r'^i18n/', include('django.conf.urls.i18n'))

*** In our template we need to add tag trans to know wich lines need to be translated like this:**
  
    {% trans "Bienvenido a nuestro sitio" %} 

*** Now we need to create the language dir and files for this we need to put this command in shell** 

    django-admin.py makemessages --locale=en

*** In the dir locale we have another dir that contain the file django.po, this file contains the words that we can tranlate:**

    templates/inicio.html:18
    msgid "Bienvenido a nuestro sitio"
    msgstr "歡迎光臨我們的網站"

    the msgid is the sentence that can be translate and the msgstr is the translate of this sentence

*** finally we need to compile the traduction with this command**

    django-admin.py compilemessages