from unipath import Path
import os

BASE_DIR = Path(__file__).ancestor(3)

SECRET_KEY = 'kvdmlo@auayq)=fo(^%(rgn=h0v*2t^(j*18_lsfp7%8xtz$%%'

DJANGO_APPS = (
	
	'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',


	)

THIRD_PARTY_APPS = (


	)

LOCAL_APPS = (

	'apps.HelloWorld',
	
	)

INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + LOCAL_APPS


MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'HWMulti.urls'

WSGI_APPLICATION = 'HWMulti.wsgi.application'

LANGUAGE_CODE = 'es'
_ = lambda s: s

LANGUAGES = (
    ('es',  _('Spanish')),
    ('zh',  _('Chinese')),
    ('en',  _('English')),
)

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

TEMPLATE_DIRS = [BASE_DIR.child('templates')]

LOCALE_PATHS = (
    os.path.join(BASE_DIR, "locale"),
)

